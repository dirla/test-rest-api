#!/bin/bash

cd /app
export GRADLE_HOME=/app/gradle-7.5.1
$GRADLE_HOME/bin/gradle checkstyleMain
$GRADLE_HOME/bin/gradle checkstyleTest
